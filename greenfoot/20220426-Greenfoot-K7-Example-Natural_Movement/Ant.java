import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Ant extends Players
{
    public int antType;
    
    public void act() 
    {
        randomMove();
    }    
    
    public void checkTouching(){
        if(this.isTouching(Ant.class)){
            Ant intersectingActor = (Ant) this.getOneIntersectingObject(Ant.class);
            System.out.println("Kollision mit "+intersectingActor.antType);
            if(intersectingActor.antType > 1){
                System.out.println("Entferne Actor");
            }
        }
    }
    public void removeTouchingAnt(){
        this.removeTouching(Ant.class);
    }
}
