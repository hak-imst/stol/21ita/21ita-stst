import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    Frog frog;
    int actSteps = 0;
    
    
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);        
        //Frog frog = new Frog();
        frog = new Frog();
        addObject(frog, 200, 200);
    }
    
    public void act(){
        actSteps = actSteps + 1;
        GreenfootImage princePic = new GreenfootImage("prince.png");
        //Frog frog = new Frog();
        //addObject(frog, 400, 300);
        
        if(actSteps > 50){
            frog.setImage(princePic);
        }
    }
}
