import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Fly extends Actor
{
    
    public void act() 
    {
        int turnDegrees = Greenfoot.getRandomNumber(90);
        if(this.isAtEdge()) {            
            this.turn(turnDegrees);
        }
        if(Greenfoot.getRandomNumber(100) < 10){
            turn(turnDegrees);
        }
        move(4);
    }    
}
