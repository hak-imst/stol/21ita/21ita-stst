import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Flower extends Actor
{
    public void act() 
    {
        // check if Actor is clicked
        // https://www.greenfoot.org/files/javadoc/greenfoot/Greenfoot.html#mouseClicked-java.lang.Object-
        if(Greenfoot.mouseClicked(this)){
            // generate random Integer between 0 and 3
            // https://www.greenfoot.org/files/javadoc/greenfoot/Greenfoot.html#getRandomNumber-int-
            int zufallszahl = Greenfoot.getRandomNumber(4);
            // print random number to console
            System.out.println(zufallszahl + 1);
        }
        
    }    
}
