import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bee here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bee extends Actor
{
       
    
    public Bee(){
        System.out.println("Ich bin die Biene");
    }
    /**
     * Act - do whatever the Bee wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        int moveDistance = 5;
        // nicht erlaubt Variable noch einmal zu deklarieren
        // int moveDistance = 6;
        // anderen Wert zuweisen ist erlaubt:
        moveDistance = 6;
        move(moveDistance);
        System.out.println("Ich bewege mich um: "+moveDistance);
    }    
}
